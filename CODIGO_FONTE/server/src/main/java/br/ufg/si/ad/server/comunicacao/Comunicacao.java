package br.ufg.si.ad.server.comunicacao;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.commons.mensagens.Envelope;
import br.ufg.si.ad.commons.utilitario.SocketUtils;
import br.ufg.si.ad.server.negocio.TabuleiroGame;

public class Comunicacao implements Runnable{

	private static final Logger LOG = LoggerFactory.getLogger(Comunicacao.class);
	private static final int PORTA_MINIMA = 14440;
	private static final int PORTA_MAXIMA = 14450;

	private static int PORTA = PORTA_MINIMA;
	private ServerSocket serverSocket;
	private Socket socket;
	private Boolean ativo = true;

	public Comunicacao() {
		LOG.info("Iniciando camada de comunicacao");

		while(serverSocket == null && PORTA != PORTA_MAXIMA) {
			try {
				LOG.debug("Tentando abrir conexão com a porta " + PORTA);
				serverSocket = new ServerSocket(PORTA);
			} catch (IOException e) {
				LOG.error("Nao foi possivel abrir um Socket Servidor na PORTA " + PORTA + ". Tentando a proxima porta! ", e);
				PORTA++;
			}
		}

		if(serverSocket == null) {
			LOG.warn("Nao foi possivel iniciar a aplicacao. Vou encerrar a execucao!");
			throw new RuntimeException();
		} else {
			LOG.debug("Conexão aberta com sucesso na porta " + PORTA);
		}

	}

	public void run() {

		while(ativo) {
			try {
				LOG.info("Aguardando nova conexao...");
				socket = serverSocket.accept();

				LOG.debug("Conexao aberta pelo host: " + socket.getInetAddress().getHostAddress());
				LOG.info("Delegando nova conexao para outra thread tratar");
				// Recebeu a comunicação, delega ela para outra thread e volta a ouvir aqui a porta principal;
				TratamentoSocket ts = new TratamentoSocket(socket);
				Thread t = new Thread(ts);
				t.start();


			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	class TratamentoSocket implements Runnable {

		private final Logger LOG = LoggerFactory.getLogger(TratamentoSocket.class);
		private Socket socket;

		TratamentoSocket(Socket socket) {
			LOG.info("Criando novo objeto tratamento à partir do socket passado");
			this.socket = socket;
		}

		public void run() {

			LOG.info("Pegando instancia em vigência do tabuleiro...");
			TabuleiroGame tabuleiro = TabuleiroGame.getTabuleiroCorrente();

			LOG.info("Extraindo envelope do Socket aberto");
			Envelope envelope = SocketUtils.receberEnvelope(socket);

			LOG.info("Passando envelope para o tabuleiro fazer suas validacoes");
			tabuleiro.receberMensagem(envelope);

			LOG.info("Terminei por aqui. Finalizo a Thread");
			Thread.currentThread().stop();

		}

	}

}
