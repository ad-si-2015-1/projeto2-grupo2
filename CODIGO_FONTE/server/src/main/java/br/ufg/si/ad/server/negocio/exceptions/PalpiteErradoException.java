package br.ufg.si.ad.server.negocio.exceptions;

/** Exce&ccedil;&atilde;o lan&ccedil;ada quando o usu&aacute;rio d&aacute; um palpite errado para o servidor */
public class PalpiteErradoException extends GameException {

	private static final long serialVersionUID = 1L;

	public PalpiteErradoException() {
		super();
	}

	public PalpiteErradoException(String message) {
		super(message);
	}


}
