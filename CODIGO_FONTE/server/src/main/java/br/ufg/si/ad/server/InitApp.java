package br.ufg.si.ad.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.server.comunicacao.Comunicacao;
import br.ufg.si.ad.server.negocio.TabuleiroGame;

/** Classe respons&aacute;vel pela inicia&ccedil;&atilde;o do servidor
 *
 * Servindo apenas como teste atualmente */
public class InitApp {

	private static final Logger LOG = LoggerFactory.getLogger(InitApp.class);

	public static void main(String[] args) {

		LOG.info("Iniciando aplicacao");

		LOG.info("Criando um tabuleiro para o jogo");

		TabuleiroGame tabuleiro = TabuleiroGame.getTabuleiroCorrente();

	}

}
