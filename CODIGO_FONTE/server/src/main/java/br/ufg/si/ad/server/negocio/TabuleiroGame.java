package br.ufg.si.ad.server.negocio;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.commons.enums.ARMAS;
import br.ufg.si.ad.commons.enums.LOCAIS;
import br.ufg.si.ad.commons.enums.SUSPEITOS;
import br.ufg.si.ad.commons.enums.TIPO_MENSAGEM;
import br.ufg.si.ad.commons.mensagens.Envelope;
import br.ufg.si.ad.commons.mensagens.Mensagem;
import br.ufg.si.ad.commons.utilitario.SocketUtils;
import br.ufg.si.ad.server.comunicacao.Comunicacao;
import br.ufg.si.ad.server.negocio.exceptions.PalpiteErradoException;

/** Principal classe de neg&oacute;cio do servidor. Gera solu&ccedil;&atilde;o do crime, persiste, analisa palpites, lan&ccedil;a respostas */
public class TabuleiroGame {

	private static final Logger LOG = LoggerFactory.getLogger(TabuleiroGame.class);
	private static TabuleiroGame tabuleiroCorrente;
	private Mensagem solucaoCrime;

	private TabuleiroGame() {
		LOG.info("Criando nova instancia de jogo");

		LOG.info("Criando solucao de crime");
		criarSolucaoCrime();

		/* Abrindo canal de comunicação com o mundo exterior */
		Comunicacao comunicacao = new Comunicacao();
		Thread t = new Thread(comunicacao);
		t.start();

	}

	public static TabuleiroGame getTabuleiroCorrente() {
		if(tabuleiroCorrente == null) {
			tabuleiroCorrente = new TabuleiroGame();
		}

		return tabuleiroCorrente;
	}

	/** M&eacute;todo respons&aacute;vel por gerar uma solu&ccedil;&atilde;o de crime aleat&oacute;ria */
	private void criarSolucaoCrime() {
		if(solucaoCrime != null) {
			LOG.warn("Esse crime já possui uma solução. Não criarei outra");
			return;
		}

		Random randomico = new Random();

		int criminoso = (randomico.nextInt( SUSPEITOS.values().length )) + 1;
		int local = (randomico.nextInt( LOCAIS.values().length )) + 1;
		int arma = (randomico.nextInt( ARMAS.values().length )) + 1;

		SUSPEITOS criminosoDefinido = SUSPEITOS.getSuspeitoPorCodigo(criminoso);
		LOCAIS localDefinido = LOCAIS.getLocalPorCodigo(local);
		ARMAS armaDefinida = ARMAS.getArmaPorCodigo(arma);

		solucaoCrime = new Mensagem(criminosoDefinido, localDefinido, armaDefinida);
		LOG.debug("Solucao do crime gerada: " + solucaoCrime);
	}

	public void receberMensagem(Envelope envelope) {

		TIPO_MENSAGEM msgRecebida = envelope.getTipo_mensagem();

		if(msgRecebida == null) {
			LOG.warn("Mensagem recebida está vazia. Abortando processo de esculta");
			return;
		}

		switch(msgRecebida) {
		case PALPITE:
			LOG.info("Recebi um palpite do jogador. Validando...");
			try {
				validarPalpite(envelope.getMensagem());
			} catch (PalpiteErradoException e) {
				LOG.error("Usuário errou o palpite. Enviando uma dica para ele", e);
				enviarDica(envelope, e);
			}
			break;
		case IDENTIFICACAO:
			LOG.info("Recebi uma solicitacao para me identificar. Enviando identificacao");

			LOG.debug("Debugando mensagem recebida: " + envelope);
			Envelope identificacao = new Envelope(TIPO_MENSAGEM.SOU_SERVIDOR, null, null);
			identificacao.setEnderecoString(envelope.getEnderecoString());
			identificacao.setPorta(envelope.getMinhaPortaEscuta());

			SocketUtils.enviarMensagem(identificacao);

			break;

		case ACUSACAO:
			LOG.info("Usuário fez uma acusação. Avaliando acusacao...");
			try {
				validarPalpite(envelope.getMensagem());

				// Se validarPalpite nao gerar Exceçao é sinal de que o cara acertou e ganhou o jogo;
				ganhouJogo(envelope);
			} catch (PalpiteErradoException e) {
				LOG.info("Usuário errou a acusação. Será eliminado do jogo!");
				eliminarUsuario(envelope);
			}

			break;

		default:
			LOG.info("Essa mensagem não está implementada na minha escuta.");
			break;
		}

	}

	/** Método responsável por aplicar a lógica para quando um usuário ganhou o jogo */
	private void ganhouJogo(Envelope envelope) {
		LOG.debug("Instância " + envelope.getEnderecoString() + " ganhou o jogo!");

		LOG.info("montando mensagem de vitória");
		Envelope envio = new Envelope(TIPO_MENSAGEM.ACUSACAO_CORRETA, null, null);
		envio.setEnderecoString(envelope.getEnderecoString());
		envio.setPorta(envelope.getPorta());

		LOG.info("Enviando mensagem de vitória");
		SocketUtils.enviarMensagem(envio);

		LOG.info("Criando nova crime para solução!");
		tabuleiroCorrente = new TabuleiroGame();

	}

	/** Método responsável por enviar a mensagem de eliminação do jogador desse tabuleiro */
	private void eliminarUsuario(Envelope envelope) {
		LOG.debug("Eliminando jogador da instância " + envelope.getEnderecoString() );

		LOG.info("Montando mensagem de eliminacao");
		Envelope envio = new Envelope(TIPO_MENSAGEM.ACUSACAO_ERRADA, null, null);
		envio.setEnderecoString(envelope.getEnderecoString());
		envio.setPorta(envelope.getPorta());

		LOG.info("Enviando mensagem de eliminacao");
		SocketUtils.enviarMensagem(envio);

	}

	private void validarPalpite(Mensagem mensagem) throws PalpiteErradoException {
		List<String> erros = new ArrayList<String>();

		if( mensagem.getArma() != solucaoCrime.getArma() ) {
			erros.add( mensagem.getArma() );
		}
		if( mensagem.getLocal() != solucaoCrime.getLocal() ) {
			erros.add( mensagem.getLocal() );
		}
		if( mensagem.getSuspeito() != solucaoCrime.getSuspeito() ) {
			erros.add( mensagem.getSuspeito() );
		}

		if( erros.size() == 0 ) {

		}

		throw new PalpiteErradoException( erros.get( new Random().nextInt( erros.size() ) ) );
	}

	private void enviarDica(Envelope envelope, PalpiteErradoException e) {

		Envelope envelopeEnvio = new Envelope(TIPO_MENSAGEM.DICA, null, e.getMessage());
		envelopeEnvio.setEnderecoString(envelope.getEnderecoString());
		envelopeEnvio.setPorta(envelope.getMinhaPortaEscuta());

		SocketUtils.enviarMensagem(envelopeEnvio);

	}

}
