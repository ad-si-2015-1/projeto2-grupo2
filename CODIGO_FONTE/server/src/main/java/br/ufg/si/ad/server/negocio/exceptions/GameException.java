package br.ufg.si.ad.server.negocio.exceptions;

/** Classe chave da hierarquia de Exce&ccedil;&otilde;es da aplica&ccedil;&atilde;o.
 *
 * Serve ao intu&iacute;to de n&atilde;o serem criadas Exce&ccedil;&otilde;es il&oacute;gicas. Tamb&eacute;m inicia a hierarquia de Exce&ccedil;&otilde;es que ser&atilde;o tradas na aplica&ccedil;&atilde;o;*/
public class GameException extends Exception {

	private static final long serialVersionUID = 1L;

	public GameException() {
		super();
	}

	public GameException(String message) {
		super(message);
	}

	public GameException(Exception e) {
		super(e);
	}
}
