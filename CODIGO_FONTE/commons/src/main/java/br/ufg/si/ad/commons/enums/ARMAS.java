package br.ufg.si.ad.commons.enums;

public enum ARMAS {

	REVOLVER(1, "Revolver"),
	FACA(2, "Faca"),
	ESCOPETA(3, "Escopeta"),
	PA(4, "Pá"),
	BOMBA(5, "Bomba"),
	VENENO(6, "Veneno"),
	TACO_BASEBOL(7, "Bastão de Basebol"),
	ESTILINGUE(8, "Estilingue / Atiradeira"),
	ADAGA(9, "Adaga"),
	GARRAFA(10, "Garrafa de vinho"),
	CORDA(11, "Corda");

	private int codigo;
	private String arma;

	ARMAS(int codigo, String arma) {
		this.codigo = codigo;
		this.arma = arma;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getArma() {
		return arma;
	}

	public static ARMAS getArmaPorCodigo(int codigo) {
		for(ARMAS armasTemp : ARMAS.values()) {
			if(armasTemp.getCodigo() == codigo) {
				return armasTemp;
			}
		}
		return null;
	}
}
