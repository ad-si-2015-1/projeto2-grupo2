package br.ufg.si.ad.commons.mensagens;

import java.io.Serializable;

import br.ufg.si.ad.commons.enums.ARMAS;
import br.ufg.si.ad.commons.enums.LOCAIS;
import br.ufg.si.ad.commons.enums.SUSPEITOS;

/** Mensagem contem uma combinacao suspeito / local / arma que será utilizado no jogo para palpites e julgamentos do crime */
public class Mensagem implements Serializable{

	private static final long serialVersionUID = -3455103907849732831L;
	private SUSPEITOS suspeito;
	private LOCAIS local;
	private ARMAS arma;

	public Mensagem(SUSPEITOS suspeito, LOCAIS local, ARMAS arma) {
		this.suspeito = suspeito;
		this.local = local;
		this.arma = arma;
	}

	public String getSuspeito() {
		return suspeito.getNome();
	}
	public String getLocal() {
		return local.getLocal();
	}
	public String getArma() {
		return arma.getArma();
	}

	public String toString() {
		return "O suspeito " + suspeito.getNome() + " cometeu o crime usando um(a) " + arma.getArma() + " no(a) " + local.getLocal();
	}
}
