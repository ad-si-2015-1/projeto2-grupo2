package br.ufg.si.ad.commons.utilitario;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.commons.mensagens.Envelope;

public class SocketUtils {

	private static final Logger LOG = LoggerFactory.getLogger(SocketUtils.class);

	public static void enviarMensagem(Envelope envelope) {

		Socket socket;

		try {
			LOG.info("Abrindo conexão com o host: " + envelope.getEnderecoString() + " na porta " + envelope.getPorta());
			socket = new Socket(envelope.getEnderecoString(), envelope.getPorta());
			LOG.info("Conexão aberta!");

			LOG.info("Enviando mensagem");
			OutputStream os = socket.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(envelope);

		} catch (UnknownHostException e) {
			LOG.error("Host desconhecido ");
		} catch (IOException e) {
			LOG.error("Problema na abertura da conexao");
		}

	}

	public static Envelope receberEnvelope(Socket socket) {
		Envelope envelope = null;

		try {
			LOG.info("Recebendo objeto da rede");
			InputStream is = socket.getInputStream();
			ObjectInputStream ois = new ObjectInputStream(is);
			envelope = (Envelope) ois.readObject();

			/* aqui eu seto as propriedades do emitente da mensagem */
			envelope.setEnderecoString(socket.getInetAddress().getHostAddress());
//			envelope.setPorta(envelope.getMinhaPortaEscuta()); /* O pacote já vem com a porta de escuta; */

		} catch (IOException e) {
			LOG.error("Host desconhecido ");
		} catch (ClassNotFoundException e) {
			LOG.error("Classe recebida não reconhecida");
		}

		return envelope;
	}
}
