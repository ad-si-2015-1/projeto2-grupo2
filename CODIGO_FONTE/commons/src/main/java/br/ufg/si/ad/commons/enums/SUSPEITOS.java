package br.ufg.si.ad.commons.enums;

public enum SUSPEITOS {

	SARGENTO_BIGODE(1, "Sargento Bigode", "Sargento do exercito da cidade é um antigo amigo do Sr. Pessoa que teve uma história de amor com a mulher de pessoa no passado"),
	FLORISTA_DONA_BRANCA(2, "Florista Dona Branca", "Depois penso em algo"),
	CHEFE_DE_COZINHA_TONY(3,"Chefe de cozinha Tony", "Fazia as comida da galera"),
	MORDOMO_JAMES(4, "Mordomo James", "Esse tinha uns motivos para meter a peixeira no véio"),
	DOUTORA_VIOLETA(5, "Doutora Violeta", "Cuidava do véio e é gostosa pra caraio"),
	DANCARIA_STA_ROSA(6, "Dançarina Sra Rosa", "Outra gostosa que alisava a careca do véio"),
	GUARDA_SERGIO_SOTURNO(7, "Guarda Sérgio Soturno", "Não confio na polícia, raça do caralho!"),
	ADVOGADO_SR_MARINHO(8, "Advogado Sr. Marinho", "Advogado... cena do Crime... parece meio suspeito, não?");

	private int codigo;
	private String nome;
	private String descricao;

	SUSPEITOS(int codigo, String nome, String descricao){
		this.codigo = codigo;
		this.nome = nome;
		this.descricao = descricao;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public static SUSPEITOS getSuspeitoPorCodigo(int codigo) {
		for(SUSPEITOS suspeito : SUSPEITOS.values()) {
			if(suspeito.getCodigo() == codigo) {
				return suspeito;
			}
		}
		return null;
	}
}
