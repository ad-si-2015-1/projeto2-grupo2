package br.ufg.si.ad.commons.enums;

/** Tipo mensagem é uma enumeração dos tipos de mensagens definidos para serem trocados na implementação do projeto;*/
public enum TIPO_MENSAGEM {

	/** PALPITE: lançado pelo cliente para o servidor. Serve a função de informar uma possibilidade de crime*/
	PALPITE,

	/** ACUSACAO: lançado pelo cliente para o servidor. Serve para apontar um resultado de um crime. Se acusação correta, vence. Se errada, perde*/
	ACUSACAO,

	/** DICA: lançado pelo servidor para o cliente. Serve para dar uma dica para o cliente sobre um palpite e o que está incorreto nele*/
	DICA,

	/** REGISTRO: lançado pelo servidor para o cliente. Serve para informar para os demais clientes o palpite ou a acusação dada por um cliente específico */
	REGISTRO,

	/** IDENTIFICACAO: mensagem mandada pelo cliente para encontrar o servidor disponivel para jogo */
	IDENTIFICACAO,

	/** SOU_SERVIDOR: mensagem que o servidor manda em resposta ao cliente sob uma solicitacao de identificacao */
	SOU_SERVIDOR,

	/** ACUSACAO_ERRADA: mensagem lançada pelo servidor quando usuário erra acusação. O player que acusou deve ser eliminado*/
	ACUSACAO_ERRADA,

	/** ACUSACAO_CORRETA: mensagem lançada pelo servidor quando usuário acerta a acusação. O player ganha o jogo e o servidor deve iniciar nova mesa de desafios; */
	ACUSACAO_CORRETA;
}
