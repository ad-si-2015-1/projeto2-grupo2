package br.ufg.si.ad.commons.enums;

public enum LOCAIS {

	SALA_ESTAR(1, "Sala de estar"),
	JARDIM(2, "Jardim da casa"),
	COZINHA(3, "Cozinha"),
	BIBLIOTECA(4, "Biblioteca"),
	QUARTO(5, "Quarto"),
	SALAO_FESTAS(6, "Salao de festas"),
	HALL(7, "Hall"),
	SALA_JANTAR(8, "Sala de jantar");

	private int codigo;
	private String local;

	LOCAIS(int codigo, String local) {
		this.codigo = codigo;
		this.local = local;
	}

	public int getCodigo() {
		return codigo;
	}
	public String getLocal() {
		return local;
	}

	public static LOCAIS getLocalPorCodigo(int codigo) {
		for(LOCAIS localTemp : LOCAIS.values()) {
			if(localTemp.getCodigo() == codigo) {
				return localTemp;
			}
		}
		return null;
	}

}
