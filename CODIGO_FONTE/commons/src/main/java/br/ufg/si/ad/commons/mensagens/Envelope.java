package br.ufg.si.ad.commons.mensagens;

import java.io.Serializable;

import br.ufg.si.ad.commons.enums.TIPO_MENSAGEM;

/** Envelope contem a combinacao de Mensagem + Tipo mensagem que será transmitida entre os lados ServerSide e ClientSide*/
public class Envelope implements Serializable{

	private static final long serialVersionUID = -4683766899369481475L;
	/* Identificadores do destinatario da mensagem */
	private String enderecoString;
	private int porta;

	/* Identificação do emitente da mensagem */
	private int minhaPortaEscuta;

	/* Informações referente à mensagem */
	private TIPO_MENSAGEM tipo_mensagem;
	private Mensagem mensagem;
	private String dica;

	public Envelope(TIPO_MENSAGEM tipo_mensagem, Mensagem mensagem, String dica) {
		this.tipo_mensagem = tipo_mensagem;
		this.mensagem = mensagem;
		if(tipo_mensagem == TIPO_MENSAGEM.DICA) {
			this.dica = dica;
		}
	}


	public String getEnderecoString() {
		return enderecoString;
	}
	public void setEnderecoString(String enderecoString) {
		this.enderecoString = enderecoString;
	}

	public int getPorta() {
		return porta;
	}
	public void setPorta(int porta) {
		this.porta = porta;
	}

	public TIPO_MENSAGEM getTipo_mensagem() {
		return tipo_mensagem;
	}
	public Mensagem getMensagem() {
		return mensagem;
	}
	public String getDica() {
		return dica;
	}
	public int getMinhaPortaEscuta() {
		return minhaPortaEscuta;
	}
	public void setMinhaPortaEscuta(int minhaPortaEscuta) {
		this.minhaPortaEscuta = minhaPortaEscuta;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("\nenderecoString: " + getEnderecoString());
		sb.append("\nporta: " + getPorta());
		sb.append("\ntipo_mensagem: " + getTipo_mensagem());
		sb.append("\nmensagem: " + getMensagem());
		sb.append("\ndica: " + getDica());

		return sb.toString();
	}

}
