package br.ufg.si.ad.client.socket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.client.negocio.ClienteGame;
import br.ufg.si.ad.commons.enums.TIPO_MENSAGEM;
import br.ufg.si.ad.commons.mensagens.Envelope;
import br.ufg.si.ad.commons.utilitario.SocketUtils;

public class Comunicacao implements Runnable {

	private static final Logger LOG = LoggerFactory.getLogger(Comunicacao.class);

	private static final int PORTA_MINIMA = 14440;
	private static final int PORTA_MAXIMA = 14450;

	private static int PORTA = PORTA_MINIMA;
	private ServerSocket serverSocket;
	private Socket socket;
	private Boolean ativo = true;

	public Comunicacao() {
		LOG.info("Iniciando camada de comunicacao");

		while(serverSocket == null && PORTA != PORTA_MAXIMA) {
			try {
				LOG.info("Tentando abrir conexão com a porta " + PORTA);
				serverSocket = new ServerSocket(PORTA);
			} catch (IOException e) {
				LOG.debug("Nao foi possivel abrir um Socket Servidor na PORTA " + PORTA + ". Tentando a proxima porta! ");
				PORTA++;
			}
		}

		if(serverSocket == null) {
			LOG.warn("Nao foi possivel iniciar a aplicacao. Vou encerrar a execucao!");
			throw new RuntimeException();
		} else {
			LOG.info("Conexão aberta com sucesso na porta " + PORTA);
		}

	}

	public void run() {

		while(ativo) {
			try {
				LOG.info("Aguardando nova conexao...");
				socket = serverSocket.accept();

				LOG.debug("Conexao aberta pelo host: " + socket.getInetAddress().getHostAddress());
				LOG.info("Delegando nova conexao para outra thread tratar");

				try {
					LOG.warn("Muitas threads em execucao. Vou aguardar 5 segundos");
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// Recebeu a comunicação, delega ela para outra thread e volta a ouvir aqui a porta principal;
				TratamentoSocket ts = new TratamentoSocket(socket);
				Thread t = new Thread(ts);
				t.start();


			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void procurarServidor() {
		Enumeration<NetworkInterface> nis = null;

		try {
			LOG.info("Capturando interfaces disponíveis na máquina");
			nis = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
			String message = "Não encontrado nenhuma interface disponível";
			LOG.warn(message);
			throw new RuntimeException(message, e);
		}

		while (nis.hasMoreElements()) {
			NetworkInterface ni = (NetworkInterface) nis.nextElement();
//			LOG.info("Pegando próxima interface");

			if(ni.getDisplayName().contains("eth") || ni.getDisplayName().contains("wlan")) {
				Enumeration<InetAddress> ias = ni.getInetAddresses();
				InetAddress ia = null;

//				LOG.debug("Varrendo portas da interface " + ni.getDisplayName());

				while (ias.hasMoreElements()) {
//					LOG.info("Pegando próxima rede dessa interface");
					ia = (InetAddress) ias.nextElement();
					varrerPortas(ia);
				}
			}
		}

	}

	private void varrerPortas(InetAddress enderecoAlvo) {

		for(int porta = PORTA_MINIMA; porta <= PORTA_MAXIMA; porta++) {

			Envelope envio = new Envelope(TIPO_MENSAGEM.IDENTIFICACAO, null, null);
			envio.setEnderecoString(enderecoAlvo.getHostAddress());
			envio.setPorta(porta);
			envio.setMinhaPortaEscuta(PORTA);

			SocketUtils.enviarMensagem(envio);

		}
	}

	public int getPorta() {
		return PORTA;
	}


	class TratamentoSocket implements Runnable {

		private Socket socket;

		TratamentoSocket(Socket socket) {
			LOG.info("Criando novo objeto tratamento à partir do socket passado");
			this.socket = socket;
		}

		public void run() {

			LOG.info("Pegando instancia em vigência do jogador...");
			ClienteGame game = ClienteGame.getClienteGame();

			LOG.info("Extraindo envelope do socket aberto");
			Envelope envelope = SocketUtils.receberEnvelope(socket);

			LOG.info("Passando envelope para o cliente fazer as validacoes");
			game.tratarMensagensRecebidas(envelope);

			LOG.info("Terminei aqui, finalizo a Thread");
			Thread.currentThread().stop();


		}

	}

}
