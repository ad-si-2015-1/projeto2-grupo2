package br.ufg.si.ad.client.views;

import javax.swing.JOptionPane;

import br.ufg.si.ad.client.model.Jogador;
import br.ufg.si.ad.commons.enums.ARMAS;
import br.ufg.si.ad.commons.enums.LOCAIS;
import br.ufg.si.ad.commons.enums.SUSPEITOS;
import br.ufg.si.ad.commons.mensagens.Mensagem;

public class Visualizacoes {

	public static Jogador criarJogador() {

		JOptionPane.showMessageDialog(null, "Hora de criar um jogador");

		String nome;

		do {
			nome = JOptionPane.showInputDialog("Digite o nome do jogador");
		} while(nome == null || nome.trim().equals(""));

		return new Jogador(nome);
	}

	public static int escolherAcao() {
		String op;
		int valorOp = 0;

		StringBuilder sb = new StringBuilder();
		sb.append("Digite a opção desejada:\n");
		sb.append("Digite 1 para lançar palpite\n");
		sb.append("Digite 2 para lançar acusação\n");
		do {
			op = JOptionPane.showInputDialog(sb);
			try {
				valorOp = Integer.valueOf(op);
			} catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Valor inválido! Digite um valor válido");
			}
		} while(valorOp < 1 || valorOp > 2);

		return valorOp;
	}

	public static Mensagem criarMensagem() {

		JOptionPane.showMessageDialog(null, "Qual a sua suspeita?" );
		Mensagem palpite = new Mensagem(escolherSuspeito(), escolherLocal(), escolherArma());

		JOptionPane.showMessageDialog(null, "Palpite criado! O criminoso " + palpite.getSuspeito() + " cometeu o crime com a arma " + palpite.getArma() + " no seguinte local: " + palpite.getLocal());

		return palpite;

	}


	private static SUSPEITOS escolherSuspeito() {
		StringBuilder sb = new StringBuilder();

		sb.append("Escolha um suspeito para acusar\n");
		sb.append("===============================\n");

		int i = 1;
		for(SUSPEITOS suspeito : SUSPEITOS.values()) {
			sb.append(i + ": " + suspeito.getNome() + "\n");
			i++;
		}

		boolean valido = false;
		int opcoes;

		do {
			opcoes = Integer.valueOf( JOptionPane.showInputDialog(sb) );
			if(opcoes < 1 || opcoes > SUSPEITOS.values().length) {
				JOptionPane.showMessageDialog(null, "Sua opção não é válida");
			} else {
				valido = true;
				return SUSPEITOS.getSuspeitoPorCodigo(opcoes);
			}
		} while(!valido);

		return null;
	}

	private static ARMAS escolherArma() {
		StringBuilder sb = new StringBuilder();

		sb.append("Escolha a arma que foi usada no crime\n");
		sb.append("=====================================\n");

		int i = 1;
		for(ARMAS arma : ARMAS.values()) {
			sb.append(i + ": " + arma.getArma() + "\n");
			i++;
		}

		boolean valido = false;
		int opcoes;

		do {
			opcoes = Integer.valueOf( JOptionPane.showInputDialog(sb) );
			if(opcoes < 1 || opcoes > ARMAS.values().length) {
				JOptionPane.showMessageDialog(null, "Sua opção não é valida");
			} else {
				valido = true;
				return ARMAS.getArmaPorCodigo(opcoes);
			}
		} while(!valido);

		return null;
	}

	private static LOCAIS escolherLocal() {
		StringBuilder sb = new StringBuilder();

		sb.append("Escolha o local onde o crime ocorreu\n");
		sb.append("====================================\n");

		int i = 1;
		for(LOCAIS local : LOCAIS.values()) {
			sb.append(i + ": " + local.getLocal() + "\n");
			i++;
		}

		boolean valido = false;
		int opcoes;

		do {
			opcoes = Integer.valueOf( JOptionPane.showInputDialog(sb) );
			if(opcoes < 1 || opcoes > LOCAIS.values().length) {
				JOptionPane.showMessageDialog(null, "Sua opção não é válida");
			} else {
				valido = true;
				return LOCAIS.getLocalPorCodigo(opcoes);
			}
		} while(!valido);

		return null;
	}
}
