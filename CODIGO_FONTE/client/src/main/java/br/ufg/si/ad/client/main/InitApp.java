package br.ufg.si.ad.client.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.client.negocio.ClienteGame;
import br.ufg.si.ad.client.views.Visualizacoes;

/** Classe que irá iniciar a aplicação
 *
 * Por enquanto o intuíto da mesma é para testes*/
public class InitApp {

	private static final Logger LOG = LoggerFactory.getLogger(InitApp.class);

	private ClienteGame game;

	InitApp() {
		LOG.info("Iniciando...");
		init();
	}

	void init() {

		LOG.info("Chamando a classe de negócio do cliente");
		// Inicia a classe de negócio
		game = ClienteGame.getClienteGame();

		if(game.getJogador() != null) {
			LOG.info("Vamos criar o jogador...");
			game.setJogador( Visualizacoes.criarJogador() );
		}

		LOG.info("Vamos iniciar a interação do jogo");
		game.interacaoJogo();

	}

	public static void main(String[] args) {

		LOG.info("Bem vindo ao client do Jogo");
		InitApp app = new InitApp();

	}

}
