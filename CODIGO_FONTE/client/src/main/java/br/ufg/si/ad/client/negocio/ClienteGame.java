package br.ufg.si.ad.client.negocio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufg.si.ad.client.model.Jogador;
import br.ufg.si.ad.client.socket.Comunicacao;
import br.ufg.si.ad.client.views.Visualizacoes;
import br.ufg.si.ad.commons.enums.TIPO_MENSAGEM;
import br.ufg.si.ad.commons.mensagens.Envelope;
import br.ufg.si.ad.commons.mensagens.Mensagem;
import br.ufg.si.ad.commons.utilitario.SocketUtils;

/** A classe ClienteGame é a classe responsável pelas regras de negócio do jogo<br>
 *
 * É nessa classe que grande parte da implementação da inteligência irá funcionar, inclusive chamando views e interações do jogo<br>
 *
 * Transferir responsabilidades de gerenciamento para cá */
public class ClienteGame {

	private static final Logger LOG = LoggerFactory.getLogger(ClienteGame.class);

	private static ClienteGame clienteGame;
	private String enderecoServidor;
	private int portaServidor;
	private Comunicacao comunicacao;
	private Comunicacao ouvir;
	private boolean ativo = true;

	/** Principal instância do jogador*/
	private Jogador jogador;

	/** Construtor principal do jogo; <br><br>
	 *
	 * Singleton porque só tem uma instância de jogo a cada execução do aplicativo */
	private ClienteGame() {

		LOG.info("Criando nova conexão do jogo");

		ouvir = new Comunicacao();
		comunicacao = new Comunicacao();

		Thread t = new Thread(ouvir);
		t.start();

		comunicacao.procurarServidor();

	}

	public static ClienteGame getClienteGame() {

		if(clienteGame == null) {
			LOG.info("Criando novo negócio de cliente");
			clienteGame= new ClienteGame();
		}

		return clienteGame;

	}

	/** Esse cara aqui só serve para ouvir respostas do servidor */
	public void tratarMensagensRecebidas(Envelope envelope) {

		TIPO_MENSAGEM msgRecebida = envelope.getTipo_mensagem();
		if(msgRecebida == null) {
			LOG.warn("Tipo de mensagem está vazia. Não sei o que fazer!");
			return;
		}
		switch (envelope.getTipo_mensagem()) {
		case SOU_SERVIDOR:
			LOG.info("Recebi as informações referentes ao servidor");
			enderecoServidor = envelope.getEnderecoString();
			portaServidor = envelope.getMinhaPortaEscuta();
			LOG.info("Informações do servidor salva nessa instância");
			break;

		default:
			LOG.warn("Não tenho implementação para esse tipo de mensagem");
			break;
		}
	}

	public Jogador getJogador() {
		return jogador;
	}
	public void setJogador(Jogador jogador) {

		if(this.jogador == null) {
			this.jogador = jogador;
		} else {
			LOG.info("Ja existe jogador instanciado. Nao pode instanciar outro;");
		}

	}

	public String getEnderecoServidor() {
		return enderecoServidor;
	}
	void setEnderecoServidor(String endereco) {
		enderecoServidor = endereco;
	}
	public int getPortaServidor() {
		return portaServidor;
	}
	void setPortaServidor(int porta) {
		portaServidor = porta;
	}

	public void interacaoJogo() {

		LOG.info("Iniciando nova iteração do jogo");
		int opcao;

		while(ativo) {
			LOG.info("Estou ativo. Vou escolher minha ação");
			opcao = Visualizacoes.escolherAcao();
			Mensagem mensagem;
			Envelope envelope;

			switch(opcao) {
			case 1:
				LOG.info("Opção escolhida: palpite");
				mensagem = Visualizacoes.criarMensagem();

				envelope = new Envelope(TIPO_MENSAGEM.PALPITE, mensagem, null);
				envelope.setMinhaPortaEscuta(comunicacao.getPorta());
				envelope.setEnderecoString(enderecoServidor);
				envelope.setPorta(portaServidor);
				LOG.debug("Debugando envelope: " + envelope);

				LOG.info("Enviando palpite para o servidor");
				SocketUtils.enviarMensagem(envelope);
				break;

			case 2:
				LOG.info("Opção escolhida: acusação");
				mensagem = Visualizacoes.criarMensagem();

				envelope = new Envelope(TIPO_MENSAGEM.PALPITE, mensagem, null);
				envelope.setMinhaPortaEscuta(comunicacao.getPorta());
				envelope.setEnderecoString(enderecoServidor);
				envelope.setPorta(portaServidor);
				LOG.debug("Debugando envelope: " + envelope);

				LOG.info("Enviando acusacao para o servidor");
				SocketUtils.enviarMensagem(envelope);

				break;

			default:
				LOG.warn("Não sei o que fazer com essa mensagem. Vou ignorar");
			}

			mensagem = null;
			envelope = null;
		}

	}


}
