package br.ufg.si.ad.client.model;

/** Jogador: <br>
 * Classe responsável por guardar informações do jogador */
public class Jogador {

	private String nome;

	public Jogador(String nome) {
		this.nome = nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
}
