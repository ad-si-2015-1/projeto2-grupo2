Detetive
=============


Nosso jogo terá a seguinte estrutura: 8 suspeitos, 8 locais e 11 armas, onde até 6 jogadores têm que solucionar um crime. Por padrão, o servidor já contém a solução do crime (um envelope) e dicas para quando o player 1, por exemplo, errar o seu palpite (Mordomo Fulano, matou Ciclano com uma bazuca na cozinha). Temos assim 704 possibilidades distintas de crime.


----------


- **Resumo**: nosso jogo solicita a cada rodada, palpites de cada player, tendo este que acertar qual suspeito cometeu o crime, assim como onde e qual arma ele utilizou. Serão portanto, 3 afirmações por palpite. Caso o player 1 erre apenas o local, por exemplo, o servidor retorna para ele uma dica (apenas para ele), e relata para os outros jogadores o palpite do player 1. 

- **Qtd. de Jogadores**: 1 ~ 6 jogadores. 

- **Regras**: um palpite por rodada para cada jogador. A qualquer momento o jogador pode deixar de fazer um palpite e sim fazer uma acusação, caso ele acerte, vence o jogo, caso erre, está eliminado.

- **Como vencer**: o jogador que acertar todas as 3 afirmações em uma acusação vence o jogo.

- **Referências**: http://pt.wikipedia.org/wiki/Detetive_%28jogo%29

----------




### Armas, Locais e Suspeitos

| Armas | Locais |     Suspeitos |
| -------- |-------- |-------- |
| Revólver   | Sala de Estar   | Sgt. Bigode   |
| Faca   | Jd. de Casa   | Florista Dona Branca   |
| Escopeta   | Cozinha   | Chef Tony   |
| Pá   | Biblioteca  | Mordomo James   |
| Bomba   | Quarto   | Doutora Violeta   |
| Veneno   | Salão de Festas  | Dançarina Sra. Rosa  |
| Taco   | Hall   | Guarda Sérgio Soturno  |
| Adaga   | Sala de Jantar   | Advogado Sr. Marinho   |
| Garrafa | - | -   |
| Corda | -   | -   |




-------------

### Colaboradores/Equipe

- Guilherme Pinheiro - guilhermepinheirosi@gmail.com
- Bruno Nogueira
- Ana Letícia
- Daniel Melo
- Douglas Oliveira

----------